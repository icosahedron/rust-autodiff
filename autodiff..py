import math
class Var:
    def __init__(self, val: float, local_gradients=()):
        self.val = val
        self.local_gradients = local_gradients
        self.grad = 0

    def backward(self, path_value: float = 1):
        for child_var, local_gradient in self.local_gradients:
            child_var.grad += path_value * local_gradient
            child_var.backward(path_value * local_gradient)

Var.__add__ = lambda a, b: Var(a.val + b.val, [(a, 1), (b, 1)])
Var.__truediv__ = lambda a, b: Var(a.val / b.val, [(a, 1 / b.val), (b, -a.val/b.val**2)])
Var.__mul__ = lambda a, b: Var(a.val * b.val, [(a, b.val), (b, a.val)])
Var.__neg__ = lambda a: Var(-a.val, [(a, -1)])
Var.__sub__ = lambda a, b: Var(a.val - b.val, [(a, 1), (b, -1)])
Var.__pow__ = lambda a, k: Var(a.val ** k.val, [(a, k.val * a.val ** (k.val - 1)), (k, (a.val ** k.val) * math.log(a.val))])
exp = lambda a: Var(math.exp(a.val), [(a, math.exp(a.val))])
log = lambda a: Var(math.log(a.val), [(a, 1/ a.val)])
sin = lambda a: Var(math.sin(a.val), [(a, math.cos(a.val))])
cos = lambda a: Var(math.cos(a.val), [(a, -math.sin(a.val))])
sig_ = lambda a: math.exp(a) / (math.exp(a) + 1)
sigmoid = lambda a: Var(sig_(a.val), [(a, sig_(a.val) * (1 - sig_(a.val)))])
relu = lambda a: Var(max(a.val, 0), [(a, (a.val > 0) * 1)])

# a, b, c = Var(2), Var(5), Var(-3)
# y = relu(log(sin(a * b) - exp(c) + Var(5)) ** a / b * cos(c) + sigmoid(a))
# y.backward()
# print(f"{y.val = }")
# print(f"{a.grad = }")
# print(f"{b.grad = }")
# print(f"{c.grad = }")

a, b = Var(3.0), Var(4.0)
c = a + b
c.backward()
print(f"{a.grad = }")
print(f"{b.grad = }")
print(f"{c.grad = }")

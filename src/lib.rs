use std::rc::Rc;
use std::ops::*;
use std::cell::RefCell;
// use ndarray::prelude::*;
// use ndarray::{OwnedRepr};

// #[derive(Debug)]
// struct Expr<T> { 
//     var : Vec<T>,
//     local_gradients : Vec<Option<Vec<(usize, T)>>>,
//     gradients : Vec<Option<T>>,
// }

#[derive(Debug, Clone, Default)]
struct Var<T> {
    value : Rc<T>,
    local_gradients : Rc<Vec<(Var<T>, Var<T>)>>,
    gradient : Rc<RefCell<Option<Var<T>>>>
}

// impl<T> Expr<T> {
//     fn var(self : &Self, value : T) -> Var<T> {
//         self.var.push(value);
//         self.local_gradients.push(None);
//         self.gradients.push(None);
//         let v = Var { expr : self, index : self.var.len() };
//         v
//     }
// }

impl<T> Var<T> 
where
    T : Constant<Output = T>
{
    fn new(v : T) -> Var<T> {
        Var { value : Rc::new(v), local_gradients : Rc::new(vec![]), gradient : Rc::new(RefCell::new(None)) }
    }

    fn constant() -> Self {
        let c = <T as Constant>::constant();
        Var::new(c)
    }
}

trait Constant {
    type Output;

    fn constant() -> Self::Output;
}

impl Constant for f64 {
    type Output = Self;

    fn constant() -> Self {
        1.0
    }
}

impl Neg for &Var<f64> {
    type Output = Var<f64>;

    fn neg(self : Self) -> Self::Output {
        let v = Var { value : Rc::new(-*self.value), 
            local_gradients : Rc::new(vec![(self.clone(), Var::new(-1.0))]), 
            gradient : Rc::new(RefCell::new(None)) };
        v
    }
}

impl Add for &Var<f64> {
    type Output = Var<f64>;

    fn add(self : Self, rhs : Self) -> Self::Output {
        let v = Var { value : Rc::new(*self.value + *rhs.value), 
            local_gradients : Rc::new(vec![(self.clone(), Var::new(1.0)), (rhs.clone(), Var::new(1.0))]), 
            gradient : Rc::new(RefCell::new(None)) };
        // let e = self.expr;
        // let sum = e.var[self.index] + e.var[rhs.index];
        // e.var.push(sum);
        // e.local_gradients.push(Some(vec![(self.index, 1.0), (rhs.index, 1.0)]));
        v
    }
}

impl Var<f64> 
{
    fn derive_tree(self : &Self, path_value : Var<f64>) {
        for (child, local_gradient) in self.local_gradients.iter() {
            let mut g = child.gradient.borrow_mut();
            let t = g.take();
            g.replace(match t {
                None => path_value.clone(),
                Some(gr) => &gr + &(&path_value * &local_gradient)
            });
            child.derive_tree(&path_value * &local_gradient);
        }
    }

    fn derive(self : &Self) {
        let path_value = Var::<f64>::constant();
        self.derive_tree(path_value)
    }
}

// impl Sub for Var<f64> {
//     type Output = Self;

//     fn sub(self : Self, rhs : Self) -> Self {
//     }
// }

impl Mul for &Var<f64> {
    type Output = Var<f64>;

    fn mul(self : Self, rhs : Self) -> Self::Output {
        let v = Var { value : Rc::new(*self.value * *rhs.value), 
            local_gradients : Rc::new(vec![(self.clone(), Var::new(1.0)), (rhs.clone(), Var::new(1.0))]), 
            gradient : Rc::new(RefCell::new(None)) };
        v
    }
}

// impl Mul for &Var<f64> {
//     type Output = Var<f64>;

//     fn mul(self : Self, rhs : Self) -> Self::Output {
//     }
// }

// impl Div for Var<f64> {
//     type Output = Self;

//     fn div(self : Self, rhs : Self) -> Self {
//     }
// }


// impl Add<Var<Vec<f64>>> for Var<Vec<f64>> {
//     type Output = Self;

//     fn add(self : Self, rhs : Self) -> Self {
//         let vec_add = (*self.value).iter().zip(rhs.value.iter()).map(|(a, b)| *a + *b).collect();
//         Var { value : Rc::new(vec_add) }
//     }
// }

// impl Add<Var<f64>> for Var<Vec<f64>> {
//     type Output = Var<Vec<f64>>;

//     fn add(self : Self, scalar : Var<f64>) -> Self::Output {
//         let vec_add = (*self.value).iter().map(|a| *a + *scalar.value).collect();
//         Var { value : Rc::new(vec_add) }
//     }
// }

// // A use case for ndarray, a 3rd party crate for 

// impl Add for Var<ArrayBase<OwnedRepr<f64>, Ix2>> {
//     type Output = Self;

//     fn add(self : Self, rhs : Self) -> Self {
//         // ndarray uses references to produce a new array
//         let c = &*self.value + &*rhs.value;
//         Var { value : Rc::new(c) }
//     }
// }

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_var_f64_add() {
        let a = Var::new(3.0);
        let b = Var::new(4.0);

        let c = &a + &b;
        assert_eq!(*c.value, 7.0, "Var<f64> + Var<f64>");
    }

    #[test]
    fn test_var_f64_derive() {
        let a = Var::new(3.0);
        let b = Var::new(4.0);

        let c = &a + &b;
        c.derive();
        println!("{:?}", a);
        println!("{:?}", b);
        println!("{:?}", c);
        println!("{:?}", a.gradient);
        println!("{:?}", b.gradient);
        println!("{:?}", c.gradient);
    }
}